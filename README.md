# Timeline

Timeline is a simple and minimalistic way of displaying current time as a nice smooth curve, eg.
this (23:06).

![23:06 Timeline](./example.png)

The shape of the curve is completely determined by the current time of the day. Current time can be
found out simply by looking at the current curve (except midnight and noon, which look the same -
straight line, but each other time has a unique curve). The algorithm is simple yet clever... you
can try to discover  it!

## Usage

Just visit the `index.html` and check it out!

You can append a few URL parameters:
- `speed=[Number]` The speed-up of the animation (eg. `speed=1440` fits all curves into a single
   minute). Default: `1`.
- `fps=[Number]` The number of updates per second (you should increase this when increasing the
   speed). Default: `5`.
- `width=[Number]` Width of the line in pixels. Default: `3`.
- `keep=[any non-empty string]` Whether previous lines should be kept when drawing a new line.
   Default: no.
- `download=[any non-empty string]` Whether a special mode for downloading many images should be
   enabled. Default: no.

When the downloading mode is enabled, the loading time of the page is significantly longer. When
the page is loaded (and a `done` message appears in the console), you can use eg. the
[Download All Images](https://addons.mozilla.org/en-US/firefox/addon/save-all-images-webextension)
extension for Firefox to download all images (just set the File Mask to `[alt]`) and use them
however you wish.
