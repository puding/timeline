/**
 * Returns value [-1, 1] of a periodic function (with specified period) similar to sin(x) that
 * consists of straight lines and sharp minima/maxima.
 */
function triangleWave(x, period) {
    x = x/period
    x = 4*(x - Math.floor(x))
    if (x > 3) return x - 4
    if (x > 1) return 2 - x
    return x
}

/** Returns current time of the day as a fraction, speeded up according to the speed parameter. */
function currentTime(start, speed) {
    const date = new Date(start + speed*(Date.now() - start))
    const time = date.getHours()*3600
        + date.getMinutes()*60
        + date.getSeconds()
        + date.getMilliseconds()/1000
    return time/86400
}

/** Takes time of the day as a fraction and draws corresponding line. */
function draw(canvas, ctx, time, keep) {
    const W = canvas.width
    const H = canvas.height

    const xControl = [-1, 1, 3, 5, 7].map(i => i*0.125)
    const yControl = [1, 1/24, 1/72, 1/12, 1/2].map(period => triangleWave(time, period)*1.25)
    const xEnd = [0, 1, 2, 3, 4].map(i => i*0.25)

    if (!keep) {
        ctx.clearRect(0, 0, W, H)
    }
    ctx.beginPath()
    ctx.moveTo(0, H/2)
    for (let i of [1, 2, 3, 4]) {
        ctx.bezierCurveTo(
            W*(2*xEnd[i-1] - xControl[i-1]),
            H/2*(1 - yControl[i-1]),
            W*xControl[i],
            H/2*(1 + yControl[i]),
            W*xEnd[i],
            H/2,
        )
    }
    ctx.stroke()
    if (time === 0) {
        ctx.fillRect(0, 0, 1, 1)
    }
}

function resetCanvas(canvas, ctx, lineWidth) {
    canvas.width = window.innerWidth
    canvas.height = 0.4*window.innerWidth
    ctx.fillStyle = 'rgba(128, 128, 128, 1%)'
    ctx.strokeStyle = '#C0C0C0'
    ctx.lineWidth = lineWidth
}

function main() {
    const canvas = document.querySelector('canvas')
    const ctx = canvas.getContext('2d')
    const start = Date.now()

    const params = new URL(window.location.href).searchParams
    const speed = parseFloat(params.get('speed')) || 1
    const fps = parseFloat(params.get('fps')) || 5
    const lineWidth = parseFloat(params.get('width')) || 3
    const keep = !!params.get('keep')
    const download = !!params.get('download')

    resetCanvas(canvas, ctx, lineWidth)
    if (download) {
        for (let hour = 0; hour < 24; hour++) {
            for (let minute = 0; minute < 60; minute++) {
                draw(canvas, ctx, (60*hour + minute)/1440)
                let img = canvas.toDataURL('image/png')
                let alt = `${hour<10 ? '0'+hour : hour}-${minute<10 ? '0'+minute : minute}`
                document.write(`<img src="${img}" alt="${alt}" style="display: none" /img>`)
            }
        }
        console.log('done')
    } else {
        const update = () => draw(canvas, ctx, currentTime(start, speed), keep)
        update()
        window.addEventListener('resize', () => {
            resetCanvas(canvas, ctx, lineWidth)
            update()
        })
        window.setInterval(update, 1000/fps)
    }
}

window.addEventListener('load', main)
